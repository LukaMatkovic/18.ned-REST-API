<?php include 'inc/db.php'; ?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="author" content="Luka Matkovic">
        <meta name="keywords" content="valuta, eur, dol, din, exchange, API">
        <title>Dnevni kurs</title>
        <link rel="icon" href="img/icon.png">
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <link href="https://fonts.googleapis.com/css?family=Song+Myung" rel="stylesheet">

    </head>
    <body>
        <div id="formWindow">
            <?php
            $iznos = $_POST['iznos'];
            $valuta = $_POST['valuta'];
            $kurs = $_POST['kurs'];

            $sql = "SELECT " .$kurs. " FROM kurs_dinara WHERE valuta_id='".$valuta."'";

            $result = mysqli_query($connection, $sql) or die(mysqli_error($connection));

            foreach($result AS $res) {
                $vrednost = $res[$kurs];
            }
            $total = $iznos*$vrednost;

            $sql_valuta = "SELECT naziv, kod FROM valuta WHERE id='". $valuta ."'";

            $result_valuta = mysqli_query($connection, $sql_valuta) or die (mysqli_error($connection));

            foreach ($result_valuta AS $res_valuta) {
                $valuta_naziv = $res_valuta['naziv'];
                $valuta_kod = $res_valuta['kod'];
            }

            echo '<p id="ispis">'.$iznos.' '.$valuta_naziv.' = '.$total.' RSD</p>';

            // CREATE TABLE konverzije (
            //     id int(10) not null PRIMARY KEY AUTO_INCREMENT,
            //     timestamp int(15) not null,
            //     iznos varchar(10) not null,
            //     valuta_kod varchar(5) not null,
            //     kurs varchar(10) not null,
            //     oneVal varchar(10) not null,
            //     total  varchar(10) not null
            //     );
            $timestamp = strtotime("now");

            $sql_konverzije = "INSERT INTO konverzije (timestamp, iznos, valuta_kod, kurs, vrednost, total)
            VALUES ('$timestamp', '$iznos', '$valuta_kod', '$kurs', '$vrednost', '$total')";

            $result_konverzije = mysqli_query($connection, $sql_konverzije) or die (mysqli_error($connection));

            ?>
            <p><a href="index.php" class="button">Povratak</a></p>
            <?php include 'inc/json.php'; ?>
            <p><a href="data/konverzije.json" target="_blank" class="button">Pogledaj JSON svih konverzija</a></p>
        </div>

    </body>
</html>
