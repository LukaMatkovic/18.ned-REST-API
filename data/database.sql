-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jun 15, 2018 at 11:51 PM
-- Server version: 5.7.19
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `06/13`
--

-- --------------------------------------------------------

--
-- Table structure for table `konverzije`
--

DROP TABLE IF EXISTS `konverzije`;
CREATE TABLE IF NOT EXISTS `konverzije` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `timestamp` int(15) NOT NULL,
  `iznos` varchar(10) NOT NULL,
  `valuta_kod` varchar(5) NOT NULL,
  `kurs` varchar(10) NOT NULL,
  `vrednost` varchar(10) NOT NULL,
  `total` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `konverzije`
--

INSERT INTO `konverzije` (`id`, `timestamp`, `iznos`, `valuta_kod`, `kurs`, `vrednost`, `total`) VALUES
(5, 1529005536, '7', 'usd', 'kupovni', '99.7648', '698.3536'),
(2, 1529005293, '10', 'eur', 'srednji', '118.1668', '1181.668'),
(3, 1529005465, '5', 'chf', 'kupovni', '101.2568', '506.284'),
(4, 1529005494, '100', 'gbp', 'prodajni', '134.4236', '13442.36'),
(6, 1529048479, '100', 'gbp', 'kupovni', '133.6194', '13361.94'),
(7, 1529056423, '2', 'chf', 'kupovni', '101.2568', '202.5136'),
(8, 1529056744, '1000', 'eur', 'srednji', '118.1668', '118166.8'),
(9, 1529057086, '10', 'aud', 'prodajni', '75.912', '759.12'),
(10, 1529057261, '10', 'aud', 'srednji', '75.6849', '756.849'),
(11, 1529057468, '3', 'chf', 'prodajni', '101.8662', '305.5986'),
(12, 1529057543, '5', 'gbp', 'srednji', '134.0215', '670.1075'),
(13, 1529057751, '1000', 'cad', 'prodajni', '77.3688', '77368.8'),
(14, 1529058504, '10', 'jpy', 'srednji', '', '0'),
(15, 1529058608, '10', 'jpy', 'prodajni', '0.923768', '9.23768'),
(16, 1529058716, '100', 'rub', 'srednji', '1.6336', '163.36'),
(17, 1529061809, '100', 'sek', 'srednji', '11.6758', '1167.58'),
(18, 1529066477, '9', 'aud', 'prodajni', '76.4969', '688.4721'),
(19, 1529066703, '4', 'rub', 'srednji', '1.6336', '6.5344'),
(20, 1529071503, '150', 'jpy', 'kupovni', '0.918242', '137.7363'),
(21, 1529089784, '3', 'cad', 'prodajni', '78.0173', '234.0519'),
(22, 1529091059, '1000', 'rub', 'srednji', '1.6336', '1633.6'),
(23, 1529091857, '10', 'cad', 'srednji', '77.7839', '777.839'),
(24, 1529095476, '2', 'jpy', 'kupovni', '0.918242', '1.836484'),
(34, 1529105962, '4', 'chf', 'srednji', '102.3313', '409.3252'),
(33, 1529105361, '13', 'sek', 'prodajni', '11.7108', '152.2404'),
(32, 1529105081, '100', 'rub', 'srednji', '1.6336', '163.36'),
(31, 1529096302, '2000', 'jpy', 'prodajni', '0.923768', '1847.536');

-- --------------------------------------------------------

--
-- Table structure for table `kurs_dinara`
--

DROP TABLE IF EXISTS `kurs_dinara`;
CREATE TABLE IF NOT EXISTS `kurs_dinara` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `datum` date NOT NULL,
  `valuta_id` varchar(6) NOT NULL,
  `kupovni` varchar(10) NOT NULL,
  `srednji` varchar(10) NOT NULL,
  `prodajni` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=73 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kurs_dinara`
--

INSERT INTO `kurs_dinara` (`id`, `datum`, `valuta_id`, `kupovni`, `srednji`, `prodajni`) VALUES
(1, '2018-06-14', '2', '117.8123', '118.1668', '118.5213'),
(2, '2018-06-14', '1', '99.7648', '100.065', '100.3652'),
(3, '2018-06-14', '5', '101.2568', '101.5615', '101.8662'),
(4, '2018-06-14', '3', '133.6194', '134.0215', '134.4236'),
(5, '2018-06-14', '4', '75.4578', '75.6849', '75.912'),
(6, '2018-06-14', '6', '76.906', '77.1374', '77.3688'),
(7, '2018-06-14', '0', '11.5908', '11.6257', '11.6606'),
(8, '2018-06-14', '0', '15.8099', '15.8575', '15.9051'),
(9, '2018-06-14', '0', '12.4556', '12.4931', '12.5306'),
(10, '2018-06-14', '0', '0.905691', '0.908416', '0.911141'),
(11, '2018-06-14', '0', '1.593', '1.5978', '1.6026'),
(12, '2018-06-14', '0', '15.5958', '15.6427', '15.6896'),
(13, '2018-06-14', '0', '', '15.9959', ''),
(14, '2018-06-14', '0', '', '331.185', ''),
(15, '2018-06-14', '0', '', '27.6213', ''),
(16, '2018-06-14', '0', '', '4.594', ''),
(17, '2018-06-14', '0', '', '0.368018', ''),
(18, '2018-06-14', '0', '', '60.4177', ''),
(48, '2018-06-15', '0', '15.8679', '15.9156', '15.9633'),
(47, '2018-06-15', '9', '1.6287', '1.6336', '1.6385'),
(46, '2018-06-15', '8', '0.918242', '0.921005', '0.923768'),
(45, '2018-06-15', '0', '12.5105', '12.5481', '12.5857'),
(44, '2018-06-15', '0', '15.8144', '15.862', '15.9096'),
(43, '2018-06-15', '7', '11.6408', '11.6758', '11.7108'),
(42, '2018-06-15', '6', '77.5505', '77.7839', '78.0173'),
(41, '2018-06-15', '4', '76.0393', '76.2681', '76.4969'),
(40, '2018-06-15', '3', '134.9033', '135.3092', '135.7151'),
(39, '2018-06-15', '5', '102.0243', '102.3313', '102.6383'),
(38, '2018-06-15', '1', '101.7951', '102.1014', '102.4077'),
(37, '2018-06-15', '2', '117.838', '118.1926', '118.5472'),
(49, '2018-06-15', '0', '', '15.9996', ''),
(50, '2018-06-15', '0', '', '337.6931', ''),
(51, '2018-06-15', '0', '', '27.5964', ''),
(52, '2018-06-15', '0', '', '4.5902', ''),
(53, '2018-06-15', '0', '', '0.365955', ''),
(54, '2018-06-15', '0', '', '60.4309', '');

-- --------------------------------------------------------

--
-- Table structure for table `valuta`
--

DROP TABLE IF EXISTS `valuta`;
CREATE TABLE IF NOT EXISTS `valuta` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `naziv` varchar(50) NOT NULL,
  `kod` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `valuta`
--

INSERT INTO `valuta` (`id`, `naziv`, `kod`) VALUES
(1, 'US Dollar', 'usd'),
(2, 'Euro', 'eur'),
(3, 'British Pound', 'gbp'),
(4, 'Australian Dollar', 'aud'),
(5, 'Swiss Franc', 'chf'),
(6, 'Canadian Dollar', 'cad'),
(7, 'Swedish Krona', 'sek'),
(8, 'Japanese Yen', 'jpy'),
(9, 'Russian ruble', 'rub');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
