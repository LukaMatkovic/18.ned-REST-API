18.nedelja - REST API
=====================
Kreiranje web stranica koja ima polje za unos iznosa i selekcionu listu za odabir valute, podaci se šaju na drugu stranicu koja ispisuje podatke nakon konverzije
-----------------------------------------------

- Kreirati stranicu koja ima selekcionu listu sa imenima valuta iz
baze podataka, polje za unos iznosa i radio tastere za odabir
tipa kursa. 
- Podaci sa forme se šalju skripti koja prima tri parametra: oznaku valute, tip kursa (prodajni, srednji i kupovni) i iznos.
- Stranica ispisuje vrednost konverzije i u bazu upisuje
svaku konverziju (tabela: id, timestamp, iznos, valuta, iznos kursa, konvertovani iznos). 
- Uraditi lep CSS. 
- Na naslovnoj stranici staviti link do druge stranice koja generise JSON od svih izvrsenih konverzija.