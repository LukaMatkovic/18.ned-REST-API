<?php include 'inc/db.php'; ?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="author" content="Luka Matkovic">
        <meta name="keywords" content="valuta, eur, dol, din, exchange, API">
        <title>Dnevni kurs</title>
        <link rel="icon" href="img/icon.png">
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <link href="https://fonts.googleapis.com/css?family=Song+Myung" rel="stylesheet">

    </head>
    <body>
        <div id="formWindow">
            <form action="convert.php" method="POST">

                <label for="iznos">Iznos</label><br>
                <input type="number" name="iznos" required><br>

                <label for="valuta">Valuta</label><br>
                <select name="valuta" required>
                    <option value="">Choose</option>
                    <?php

                    $sql = "SELECT * FROM valuta ORDER BY naziv ASC";
                    $result = mysqli_query($connection, $sql) or die(mysqli_error());
                    var_dump($result);
                    if (mysqli_num_rows($result)>0) {
                        while ($record = mysqli_fetch_array($result, MYSQLI_BOTH))
                        echo "<option value=\"$record[id]\">$record[naziv]</option>";
                    }

                    ?>
                </select><br>

                <label for="kurs">Kurs</label><br>
                <input type="radio" id="radio" name="kurs" value="kupovni">Kupovni<br>
                <input type="radio" id="radio" name="kurs" value="srednji">Srednji<br>
                <input type="radio" id="radio" name="kurs" value="prodajni">Prodajni<br>

                <input type="submit" name="convert" id="convert" value="Convert" class="button"><br>

            </form>
            <?php include 'inc/json.php'; ?>
            <a href="data/konverzije.json" target="_blank" class="button">Pogledaj JSON svih konverzija</a>
        </div>

    </body>
</html>
