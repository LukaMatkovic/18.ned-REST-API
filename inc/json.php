<?php 

$sql_json = "SELECT * FROM konverzije INNER JOIN valuta ON konverzije.valuta_kod=valuta.kod ORDER BY timestamp"; 
$sql_valnaziv = "SELECT naziv FROM valuta";
$result_json = mysqli_query($connection, $sql_json) or die(mysqli_error());
$result_valnaziv = mysqli_query($connection, $sql_valnaziv) or die(mysqli_error());


$konverzije = array();
$response = array();
$dates = array();

while ($record = mysqli_fetch_array($result_json, MYSQLI_BOTH)) {
    $iznos = $record['iznos'];
    $valuta = $record['naziv'];
    $kurs = $record['kurs'];
    $vrednost = $record['vrednost'];
    $total = $record['total'];
    $timestamp = $record['timestamp'];
    $dateTime = date('d.m.Y H:i:s', $timestamp);
    $date = explode(" ", $dateTime)[0];

    if (!in_array($date, $dates, true)) {
        array_push($dates,$date);
    }

    $konverzije[] = array('iznos'=>$iznos, 'valuta'=>$valuta, 'kurs'=>$kurs, 'vrednost'=>$vrednost, 'total'=>$total, 'datum'=>$date);
    
    for ($i=0; $i<count($dates); $i++) {

        unset($dnevne_konverzije);
        $dnevne_konverzije = array();

        foreach ($konverzije as $konverzija) {
            if ($dates[$i]==$konverzija['datum']) {
                array_pop($konverzija);
                array_push($dnevne_konverzije,$konverzija);
            }
        }
        $response[$i] = [
            'datum'=> $dates[$i],
            'konverzije'=> $dnevne_konverzije
        ];
    }
}

$json = fopen('data/konverzije.json', 'w');
fwrite($json, json_encode($response,JSON_PRETTY_PRINT));
fclose($json);

?> 